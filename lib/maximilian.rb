# frozen_string_literal: true

require_relative "maximilian/version"

module Maximilian
  class Error < StandardError; end
  # Your code goes here...
end
